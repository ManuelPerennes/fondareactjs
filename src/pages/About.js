import React from 'react';
import Logo from '../Components/Logo';
import Navigation from '../Components/Navigation';

const About = () => {
  return (
    <div>
      <Navigation />
      <Logo />
      <h1>A propos</h1>
      <br />
      <p>Site vitrine. Projet réalisé avec ReactJS et API.</p>
    </div>
  );
};

export default About;
