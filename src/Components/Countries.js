import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Card from './Card';

const Countries = () => {
  const [data, setData] = useState([]);
  const [sorteData, setSorteData] = useState([]);
  const [playOnce, setplayOnce] = useState(true);
  const [rangeValue, setRangeValue] = useState(30);
  const [selectedRadio, setselectedRadio] = useState('');
  const radio = ['Africa', 'America', 'Asia', 'Europe', 'Oceanie'];

  useEffect(() => {
    if (playOnce) {
      axios
        .get(
          'https://restcountries.eu/rest/v2/all?fields=name;population;region;capital;flag'
        )
        .then((res) => {
          setData(res.data);
          setplayOnce(false);
        });
    }

    const sortedCountry = () => {
      const countryObj = Object.keys(data).map((i) => data[i]);
      const sortedArray = countryObj.sort((a, b) => {
        return b.population - a.population;
      });
      sortedArray.length = rangeValue;
      setSorteData(sortedArray);
    };
    sortedCountry();
  }, [data, playOnce, rangeValue]);

  return (
    <div className="countries">
      <div className="sort-container">
        <input
          type="range"
          min="1"
          max="250"
          value={rangeValue}
          onChange={(e) => setRangeValue(e.target.value)}
        ></input>
        <ul>
          {radio.map((radio) => {
            return (
              <li key={radio}>
                <input
                  type="radio"
                  value={radio}
                  id={radio}
                  checked={radio === selectedRadio}
                  onChange={(e) => setselectedRadio(e.target.value)}
                />
                <label htmlFor={radio}>{radio}</label>
              </li>
            );
          })}
        </ul>
      </div>
      <div className="cancel">
        {selectedRadio && (
          <h5 onClick={() => setselectedRadio('')}>Annuler Recherche</h5>
        )}
      </div>
      <ul className="countries-list">
        {sorteData
          .filter((country) => country.region.includes(selectedRadio))
          .map((country) => (
            <Card country={country} key={country.name} />
          ))}
      </ul>
    </div>
  );
};

export default Countries;
