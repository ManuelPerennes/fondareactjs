import React from 'react';

const Logo = () => {
  return (
    <div className="logo">
      <img src="./image/logo1.png" alt="logo1" />
      <h3>React World</h3>
    </div>
  );
};

export default Logo;
